# JATHub にスクラッチからUbuntuをbootさせる方法

### 0. 準備するもの
- SD card
- boot file
<details>
<summary>bootファイルから作りたいとき</summary>
# JATHub boot file の作り方。

GN-2005-1 (JAT49, JAT53)

Share the source files of Zynq design for JATHub Prototype in this project.  
Developers are supposed to be used to dealing with vivado and petalinux.

Software source file is `JAT_ubuntu.bsp`.  
Hardware design file is `JAT_ubuntu.tcl`.
Already compiled boot files are in `ready_to_boot`.

Boot test has done with JATHub prototype-0 in the U-tokyo Testbench.

## Development Environment
- Ubuntu 18.04.6 LTS on Intel 48 cpus (64 GB memory, 1 TB storage)
	- Vivado IDE 2020.2
	- Vitis IDE 2020.2
	- Petalinux 2020.2

## How to use
### git usage in a command-line
- `ssh://git@gitlab.cern.ch:7999/ynarukaw/jathub-ubuntu-boot-from-scrach.git`
- (if you need) `git branch -a`
- (if you need) `git checkout -b <your branch name>`
- (if you need, reflect changes to your local repogitory) `git pull`
- update files in your local repogitory
- ※ do NOT change the structure of the master branch (to be discussed at the time of marge request)
- `git add .`
- `git commit -m "your comments"`
- `git push`

### How to make Hardware design file (bitstream file)
- Launch Vivado
- Select "RTL project" -> Next
- Family: Zynq7000 -> Parts: xc7z045ffg900-2 -> Next -> Finish
- Flow NavigatorからCreate Block Design。名前はそのままでOK。
- Diagramペイン(画面)で+をクリック。ZynqでサーチしてZYNQ7 Processing Systemを選択。
- 置かれたプロセッサのインスタンスをダブルクリックしてカスタマイズ。
- PresetsからApply configuration...。`(git) JAT_ubuntu.tcl`を選択。
- Diagramに戻り、Run Block Automationをクリック。OK。
- `M_AXI_GP0_ACLK`と`FCLK_CLK0`を接続。
- ここでチェックマークアイコンをクリックしてデザインを検証。問題ないはず。
- ブロックデザインを保存。Sourcesタブを開いて`design_1(design_1.bd)`を選択。右クリックからCreaet HDL Wrappter。OK。
- 'Flow Navigator' -> 'PROGRAM AND DEBUG' -> click 'Generate Bitstream'
- 'File' -> 'Export' -> click 'Export Hardware'
- fill check in "include bitstream file" -> `jathub.xsa` -> Finish

### How to update PS Preset design in git
- PS module をダブルクリック
- `Presets` -> `Export configuration` -> `JAT_ubuntu.tcl` -> Finish
- update the following files in your git local repogitory
    - `(git) JAT_ubuntu.tcl`

### How to make the Boot file for the JATHub
- export `<vivado project path>/jathub.xsa` to your Ubuntu work directory
- export `(git) JAT_ubuntu.bsp` to the Ubuntu work directory
- go to the Ubuntu work directory
- `source /tools/Xilinx/petalinux/2020.2/setting.sh` (be ready to use petalinux)
- `petalinux-create -t project -n <your project name> -s JAT_ubuntu.bsp`
- `petalinux-config --get-hw-description <vivado project path>/jathub.xsa`
- 'exit' from pop-up screen
- `petalinux-build`
- `cd <your project name>/images/linux`
- `petalinux-package --boot --fsbl zynq_fsbl.elf --fpga system.bit --u-boot`
- export "BOOT.BIN", "image.ub", "boot.scr" and "uEnv.txt" to SD card (fat32 formatted partition)
- set IP address ,MAC address,... in the uEnv.txt 

### How to update Software design in git
- check if your petalinux project can compile the boot files without any error
- `cd <your project name>`
- `petalinux-package --bsp -o ./JAT_ubuntu.bsp -p .`
- find the following files in your Ubuntu work directory
    - `<your project name>/JAT_ubuntu.bsp`
- update the following files in your git local repogitory
    - `(git) JAT_ubuntu.bsp`
- leave the comment on what did you do at the petalinux configuration, when you commit your changes

</details>

### 1. Ubuntu rootfsの構築
- SDカードを二つのパーティションに分ける。
    -  第1パーティション　: boot file用
        - ファイルシステム fat32
        - サイズ 64MiB
    -  第2パーティション: Ubuntu rootfs用
        - ファイルシステム　ext4
        - サイズ 残っている容量全部

<details>
<summary>パーティションの仕方</summary>

- PCにSDを挿入。コンピュータがSD cardをどう認識したか調べる。
    `dmesg | tail`
- fdiskを用いてパーティション。終わったらSD card一旦抜き差し
     `sudo fdisk /dev/sdc`
- 作成したパーティションのフォーマット
    `sudo mkfs.vfat -F 32 /dev/sdc1`
    `sudo mkfs.ext4 /dev/sdc2`
- フォーマットできたか確認
    `sudo parted /dev/sdc print`
- 写真のようになっていたらOK
<img src="/uploads/ad8a2588275df494f6356902658240bf/スクリーンショット_2022-09-01_18.39.04.png" width="30%">

</details>

- 第2パーティションにUbuntu rootfsを構築
    -  `sudo mount /dev/sdc2 /mnt/sd2`
    - `sudo debootstrap --foreign --arch=armhf bionic /mnt/sd2 http://ports.ubuntu.com/`

- Ubuntuの初期設定
    - `sudo cp /usr/bin/qemu-arm-static /mnt/sd2/usr/bin/ `
    - `sudo chroot /mnt/sd2`

<details>
<summary>Ubuntuの中で以下のことを初期設定</summary>

- debootstrapの続きを進める。
- `./debootstrap/debootstrap --second-stage`
- ユーザ設定、パスワード設定
    - `passwd `
    - `su`
    - `passwd`
    - `adduser hoge`
    - ` usermod -aG sudo hoge`
    - `cat /etc/group | grep hoge `　hogeがいることを確認。
- 最初に入れておくと便利なものを入れておく。
    - `apt install vim`
    - `apt install emacs`
    - `apt install build-essential`
    - `apt install git`
    - `apt install openssh-server`
    - `apt install dnsutils`
    - `apt install iproute2`
    - `apt install -y net-tools`
    - python3.6.5は元から入っている。(bionicの場合)
- ネットワーク設定もしておくと良い。
    - netplanを使ってyamlファイルを作ったら、`sudo netplan apply`
- これらの設定が終わったらexitで抜ける。

</details>

### 2. bootファイルを入れる

- bootファイルを任意のディレクトリに用意。
    - git clone https://gitlab.cern.ch/ynarukaw/jathub-ubuntu-boot-from-scrach.git

- SD card第２パーティションにboot fileを入れる。
    - `sudo mount /dev/sdc1 /mnt/sd1`
    - `sudo cp BOOT.BIN boo.scr image.ub uEnv.txt /mnt/sd1`
    - `sudo umount /mnt/sd1`

### 3. JATHubでbootさせる

- 作成したSD cardをJATHubに挿して電源を入れる。
- uart経由でubuntuに入る。
    - 自分のmacにuartをつないで以下のコマンドを実行。
    - `sudo screen /dev/tty.usbserial-0001 115200`














